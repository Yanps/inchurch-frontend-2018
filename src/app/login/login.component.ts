import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, AbstractControl, Validators } from '@angular/forms';
import { OauthService } from '../shared/services/oauth.service';
import { MzToastService } from 'ngx-materialize';
import { LoaderService } from '../shared/services/loader.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginForm             : FormGroup;
  loginFormEmail        : AbstractControl;
  loginFormPassword     : AbstractControl;
  errorMessageResources : any;

  constructor(private fb            : FormBuilder,
              private oauthService  : OauthService,
              private toastService  : MzToastService,
              private loaderService : LoaderService,
              private router        : Router) {  
    
    this.loginForm = this.fb.group({ // Initialize the loginForm FormGroup
      'email'   : ['', Validators.compose([Validators.required, Validators.email])],
      'password': ['', Validators.required]
    });

    // Initialize form controls of loginForm
    this.loginFormEmail = this.loginForm.controls['email'];
    this.loginFormPassword = this.loginForm.controls['password'];

    // Setting error messages
    this.errorMessageResources = {
      email: {
        required: 'This field is required',
        email: 'Please, enter a valid email address'
      },
      password: {
        required: 'This fild is required'
      }
    };

  }

  ngOnInit() {
  }

  /**
  * Realizes a user login
  * 
  * @example
  * login()
  * 
  * @param {string} loginForm a formGroup that contains login data
  *  
  */

  login(loginForm: FormGroup): void {
    this.loaderService.setLoader('login');
    this.oauthService.login(loginForm.value.email, loginForm.value.password)
      .subscribe(
        success => {
          localStorage.setItem('app_auth_tkn', success.token);
          this.toastService.show('Successful login', 3000, 'green');
          this.router.navigate(['users/1']);
        },
        err => {
          this.toastService.show(err, 1500, 'red darken-1');
          this.loaderService.removeLoader('login');
        },
        () => {
          this.loaderService.removeLoader('login');
        }
      );
  } 

}
