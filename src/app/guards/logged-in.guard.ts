import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { OauthService } from '../shared/services/oauth.service';


@Injectable({
  providedIn: 'root'
})
export class LoggedInGuard implements CanActivate {

  constructor(private oauthService : OauthService,
              private router       : Router) {}

  
  /**
  * Verify if a user is logged in on app, and if it's false
  * the user will be redirect for the login page
  * 
  * @example
  * isLoggedIn()
  * 
  * @returns Returns a boolean value that confirms or not that the current user is Logged in
  * 
  */

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
      const isLoggedIn = this.oauthService.isLoggedIn();
      const isLoggedOutPage = next.routeConfig.path === 'login';

      if (isLoggedOutPage) {
        if (isLoggedIn) {
          this.router.navigate(['users/1']);
        }

        return !isLoggedIn;

      } else {
        if (!isLoggedIn) {
          this.router.navigate(['login']);
        }

        return isLoggedIn;
      
      }

      
  }
}
