import { Injectable } from '@angular/core';
import { HttpService } from './http.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class OauthService {

  constructor(private httpService   : HttpService) { }

  /**
  * Verify if a user is logged in on app
  * 
  * @example
  * isLoggedIn()
  * 
  * @returns Returns a boolean which confirms if a user is Logged or not
  * 
  */

  isLoggedIn(): boolean {
    return localStorage.getItem('app_auth_tkn') ? true : false;
  }


  /**
  * Realizes a user login
  * 
  * @example
  * login(\'test@test.com\', \'123456\')
  * 
  * @param {string} username username (email)
  * @param {string} password user password
  *  
  * @returns Returns a promise observable that can be subscribed
  * 
  */
  
  login(username: string, password: string): Observable<any> {
    return this.httpService.createHttpObservable('login', 'POST', {'email': username, 'password': password});
  }

}
