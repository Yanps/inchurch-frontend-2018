import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LoaderService {

  constructor() { }

  /**
  * Setting a global loader
  * 
  * @example
  * setLoader(/'login/')
  * 
  * @param {string} method method name
  *  
  */
 
  setLoader(method: string): void  {
    const loaderElement = document.querySelector('.loader');
    loaderElement.classList.remove('loader-hide');
    loaderElement.classList.add(`loading-${method}`);
  }


  /**
  * Removing a global loader
  * 
  * @example
  * removeLoader(/'login/')
  * 
  * @param {string} method method name
  *  
  */

  removeLoader(method: string): void {
    const loaderElement = document.querySelector('.loader');
    loaderElement.classList.remove(`loading-${method}`);
    loaderElement.classList.add('loader-hide');
  }

}
