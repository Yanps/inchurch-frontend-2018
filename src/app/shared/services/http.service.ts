import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  private apiURL: string = 'https://reqres.in/api/';

  constructor() { }

  /**
  * Setting default header for requests
  * 
  * @example
  * getDefaultHeaders()
  * 
  * @returns An default headers object
  */
  
  private getDefaultHeaders(): any {
    return {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
    };
  }

  /**
  * Creates and returns an promise Observable that makes a request
  * when subscribes it
  * 
  * @example
  * createHttpObservable(\'users\', \'GET\')
  * 
  * @param {string} resource Endpoint of resource
  * @param {string} method Http rerquest method
  * @param {any} requestBody Body request
  * 
  * @returns Returns a promise observable 
  * 
  */

  createHttpObservable(
    resource: string, 
    method:   string, 
    requestBody?: any): Observable<any> { 
        
    return Observable.create(observer => {
      const url = `${this.apiURL}${resource}`; //  setting resource url
      const controller = new AbortController();  // creating a abort controller for cancel request
      const signal = controller.signal; // define request signal
      
      const requestOptions = { // define request options object
        method: method,
        signal: signal,
        headers: this.getDefaultHeaders(),
        body: JSON.stringify(requestBody)
      };

      // using fetch and control observable
      fetch(url, requestOptions) 
        .then(response => {
          if (response.ok) {
            return response.json();
          } else {
            observer.error('Request failed with status code: ' + response.status);
          }
        })
        .then(body => {
          observer.next(body);
          observer.complete();
        })
        .catch(err => observer.error(err))

      return () => controller.abort();

    });
  }

}
