import { Injectable } from '@angular/core';
import { HttpService } from '../shared/services/http.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private httpService: HttpService) { }

  /**
  * Get a users list
  * 
  * @example
  * getUsers(1, 20);
  * 
  * @param {number} page page number
  * @param {number} perPage number of items per page
  *  
  * @returns Returns a promise observable that can be subscribed
  * 
  */

  getUsers(page: number, perPage?: number): Observable<any> {
    return this.httpService.createHttpObservable(`users?page=${page}&per_page=${perPage}`, 'GET');
  }


  /**
  * Remove a user
  * 
  * @example
  * deleteUser(6)
  * 
  * @param {number} userId user's id
  *  
  * @returns Returns a promise observable that can be subscribed
  * 
  */

  deleteUser(userId: number): Observable<any> {
    return this.httpService.createHttpObservable(`users/${userId}`, 'DELETE');
  }

  
  /**
  * Update a user info
  * 
  * @example
  * updateUser({\"name\": \"morpheus\", \"job\": \"zion resident\"});
  * 
  * @param {number} userId user's id
  * @param {any} userData user's info
  *  
  * @returns Returns a promise observable that can be subscribed
  * 
  */

  updateUser(userId: number, userData: any): Observable<any> {
    return this.httpService.createHttpObservable(`users/${userId}`, 'PUT', userData);
  }


  /**
  * Create a new user
  * 
  * @example
  * createUser({\"name\": \"morpheus\", \"job\": \"zion resident\"});
  * 
  * @param {any} userData user's info
  *  
  * @returns Returns a promise observable that can be subscribed
  * 
  */

  createUser(userData: any): Observable<any> {
    return this.httpService.createHttpObservable(`users`, 'POST', userData);
  }

} 
