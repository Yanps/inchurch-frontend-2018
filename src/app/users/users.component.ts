import { Component, OnInit } from '@angular/core';
import { UserService } from './user.service';
import { User } from './user.model';
import { ActivatedRoute, Router } from '@angular/router';
import { LoaderService } from '../shared/services/loader.service';
import { MzModalService } from 'ngx-materialize'
import { DeleteUserComponent } from './delete-user/delete-user.component';
import { UserUpdateComponent } from './user-update/user-update.component';
import { UserCreateComponent } from './user-create/user-create.component';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {

  users      : User;
  page       : number;
  perPage    : number;
  totalItems : number; 

  constructor(private userService    : UserService,
              private loaderService  : LoaderService,
              private activatedRoute : ActivatedRoute,
              private router         : Router,
              private mzModalService : MzModalService) {

    this.perPage = 5;
    this.totalItems = 1;
  }

  ngOnInit() {

    this.activatedRoute.params.subscribe(
      param => {
        this.page = parseInt(param.page);
        this.getUsers(this.page, this.perPage);
      }
    );
    
  }


  /**
  * Get a users list
  * 
  * @example
  * getUsers(1, 5);
  * 
  * @param {number} page page number
  * @param {number} perPage number of items per page
  * 
  */

  getUsers(page: number, perPage: number): void {
    this.loaderService.setLoader('getUsers');
    this.userService.getUsers(page, perPage)
      .subscribe(
        success =>  {
          this.users = success.data
          this.totalItems = success.total;
          this.loaderService.removeLoader('getUsers');
        }
      )
  }


  /**
  * Change a currentPage
  * 
  * @example
  * onPageChange();
  * 
  */

  onPageChange($event): void {
    this.router.navigate([`users/${parseInt($event)}`]);
  }


  /**
  * Call delete user Modal
  * 
  * @example
  * deleteUser()
  *  
  * @param {number} userId current user Id
  * 
  */

  deleteUser(userId: number): void {
    this.mzModalService.open(DeleteUserComponent, {userId: userId});
  }


  /**
  * Call update user Modal
  * 
  * @example
  * deleteUser()
  *  
  * @param {number} userId current user Id
  * 
  */

  updateUser(userId: number) : void {
    this.mzModalService.open(UserUpdateComponent, {userId: userId});
  }


  /**
  * Call create new user Modal
  * 
  * @example
  * createUser()
  * 
  */

  createUser() : void {
    this.mzModalService.open(UserCreateComponent);
  }


  logout(): void {
    localStorage.removeItem('app_auth_tkn');
    window.location.reload();
  }

}
