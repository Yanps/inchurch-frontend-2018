import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { MzBaseModal, MzModalComponent, MzToastService } from 'ngx-materialize';
import { UserService } from '../user.service';
import { LoaderService } from 'src/app/shared/services/loader.service';

@Component({
  selector: 'app-delete-user',
  templateUrl: './delete-user.component.html',
  styleUrls: ['./delete-user.component.scss']
})
export class DeleteUserComponent extends MzBaseModal implements OnInit {

  @Input() userId: number;
  @ViewChild('modal') modal: MzModalComponent;

  modalOptions: Materialize.ModalOptions = {
    dismissible: false, 
    opacity: .5,
    inDuration: 300, 
    outDuration: 200, 
    startingTop: '100%', 
    endingTop: '10%'
  };


  constructor(private userService   : UserService,
              private loaderService : LoaderService,
              private toastService  : MzToastService) { 
    super();
  }

  ngOnInit() {
  }

  /**
  * Delete current user
  * 
  * @example
  * deleteUser()
  *  
  * @param {number} userId current user Id
  * 
  */

  deleteUser(): void { // the response tratament was replicated, cause 'reqres.in' returns always 204 status for delete calls
                      // in real world app, each response would be trated separately
    this.loaderService.setLoader('deleteUser');
    this.userService.deleteUser(this.userId)
      .subscribe(
        success => {
          this.toastService.show('User Successfully removed', 1500, 'green', () => {
            this.modal.closeModal();
          });
          this.loaderService.removeLoader('deleteUser');
        },
        err => { 
          this.toastService.show('User Successfully removed', 1500, 'green', () => {
            this.modal.closeModal();
            window.location.reload();
          });
          this.loaderService.removeLoader('deleteUser');
        }
      )
  }

}
