import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { MzBaseModal, MzModalComponent, MzToastService } from 'ngx-materialize';
import { UserService } from '../user.service';
import { LoaderService } from 'src/app/shared/services/loader.service';
import { FormBuilder, FormGroup, AbstractControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-user-create',
  templateUrl: './user-create.component.html',
  styleUrls: ['./user-create.component.scss']
})
export class UserCreateComponent extends MzBaseModal implements OnInit {

  @ViewChild('modal') modal: MzModalComponent;

  createUserForm : FormGroup;
  name           : AbstractControl;
  email          : AbstractControl;

  constructor(private userService   : UserService,
              private loaderService : LoaderService,
              private toastService  : MzToastService,
              private fb            : FormBuilder) {  
    super();

    this.createUserForm = this.fb.group({
      'name' : [''],
      'email': ['']
    });

    this.name = this.createUserForm.controls['name'];
    this.email = this.createUserForm.controls['email'];
  }

  ngOnInit() {
  }


  /**
  * Create a new user
  * 
  * @example
  * createUser(userData)
  *  
  * @param {any} userData new user's info
  * 
  */

  createUser(userData: any): void {
    const userObj = {
      'name':  userData.value.name,
      'email': userData.value.name
    };

    this.loaderService.setLoader('createUser');
    this.userService.createUser(userObj)
      .subscribe(
        () => {
          this.toastService.show('New user created!', 1500, 'green', () => {
            this.modal.closeModal();
            window.location.reload();
          });
          this.loaderService.removeLoader('createUser');
        }
      )

  }

}
