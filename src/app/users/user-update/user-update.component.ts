import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { MzBaseModal, MzModalComponent, MzToastService } from 'ngx-materialize';
import { UserService } from '../user.service';
import { LoaderService } from 'src/app/shared/services/loader.service';
import { FormBuilder, FormGroup, AbstractControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-user-update',
  templateUrl: './user-update.component.html',
  styleUrls: ['./user-update.component.scss']
})
export class UserUpdateComponent extends MzBaseModal implements OnInit {

  @Input() userId: number;
  @ViewChild('modal') modal: MzModalComponent;

  updateUserForm : FormGroup;
  name           : AbstractControl;
  email          : AbstractControl;

  constructor(private userService   : UserService,
              private loaderService : LoaderService,
              private toastService  : MzToastService,
              private fb            : FormBuilder) {  
    super();

    this.updateUserForm = this.fb.group({
      'name' : [''],
      'email': ['']
    });

    this.name = this.updateUserForm.controls['name'];
    this.email = this.updateUserForm.controls['email'];
  }

  ngOnInit() {
  }


  /**
  * Update current user
  * 
  * @example
  * updateUser()
  *  
  * @param {FormGroup} userData From group that contains user info
  * 
  */

  updateUser(userData: FormGroup): void {
    const userObj = {
      'name':  userData.value.name,
      'email': userData.value.name
    };

    this.loaderService.setLoader('updateUser');
    this.userService.updateUser(this.userId, userObj)
      .subscribe(
        () => {
          this.toastService.show('User upadated', 1500, 'green', () => {
            this.modal.closeModal();
            window.location.reload();
          });
          this.loaderService.removeLoader('updateUser');
        }
      )

  }

}
