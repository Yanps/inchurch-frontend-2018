import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';


// Materialize imports
import { MzInputModule,
         MzButtonModule,
         MzValidationModule,
         MzToastModule,
         MzNavbarModule,
         MzPaginationModule,
         MzModalModule  } from 'ngx-materialize'

// Application imports
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { UsersComponent } from './users/users.component';
import { DeleteUserComponent } from './users/delete-user/delete-user.component';
import { UserUpdateComponent } from './users/user-update/user-update.component';
import { UserCreateComponent } from './users/user-create/user-create.component';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    UsersComponent,
    DeleteUserComponent,
    UserUpdateComponent,
    UserCreateComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    AppRoutingModule,
    MzInputModule,
    MzButtonModule,
    MzValidationModule,
    MzToastModule,
    MzNavbarModule,
    MzPaginationModule,
    MzModalModule 
  ],
  providers: [],
  bootstrap: [AppComponent],
  entryComponents: [DeleteUserComponent, UserUpdateComponent, UserCreateComponent]
})
export class AppModule { }
